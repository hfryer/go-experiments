package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"go_server/pkg"
)

func main() {
	log.SetLevel(log.TraceLevel)
	pkg.Test("query", 1)

	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		log.Info("message")
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	err := r.Run(":4040") // listen and serve on 0.0.0.0:8080
	fmt.Println(err)
}
